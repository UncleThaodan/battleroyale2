### Pre-game loop logic ###
#Handle players that have just re-joined the server.
execute as @a[tag=participant,scores={br.is_rejoining=1..}] run function battle_royale:events/player_rejoins_server

### Game loop ###
execute if score game_is_ongoing br.value matches 1 run function battle_royale:running/main

### Post game-loop logic ###
#Increment server time.
scoreboard players add server_time_in_ticks br.value 1

#Set everyone's "last online" timestamp
scoreboard players operation @a br.last_online = server_time_in_ticks br.value