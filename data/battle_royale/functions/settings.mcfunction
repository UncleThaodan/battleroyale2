######## Leaving Game ########
# How many ticks the player can leave the server at a time without being disqualified
scoreboard players set max_single_leave_before_disqualified br.value 300

# Total number of ticks the player can be absent for in a game (cumulative)
scoreboard players set total_leave_before_disqualified br.value 300

# Number of ticks the player is not allowed to disconnect for after taking damage from another player
scoreboard players set pvp_logging_timeout br.value 200

######## Game Properties ########
# Max duration of a match in minutes
scoreboard players set max_game_duration_in_minutes br.value 25


######## Worldborder Properties ########
worldborder damage buffer 0
worldborder warning distance 30


######## Lobby Properties ########
scoreboard players set countdown_before_game_start_in_seconds br.value 45


######## Server Settings ########
# Required in order to instantly enter spectator mode on death
gamerule doImmediateRespawn true