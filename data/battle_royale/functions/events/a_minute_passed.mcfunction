# Take 1 minute off the timer
scoreboard players remove remaining_game_duration_in_minutes br.value 1

#Update worldborder and its timers
function battle_royale:running/worldborder/on_minute_interval

#Re-shedule self
schedule function battle_royale:events/a_minute_passed 60s