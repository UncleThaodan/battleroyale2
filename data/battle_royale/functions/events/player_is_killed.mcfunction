#Reset advancement trigger
advancement revoke @s only battle_royale:death_detection

#Switch to spectator
tag @s add spectator
gamemode spectator @s

function battle_royale:disqualified/killed