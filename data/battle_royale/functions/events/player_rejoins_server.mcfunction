#Reset leave counter
scoreboard players reset @s br.is_rejoining

#Calculate time delta
scoreboard players operation delta var = server_time_in_ticks br.value
scoreboard players operation delta var -= @s br.last_online
scoreboard players operation @s br.absent_for += delta var

#Check if thresholds are exceded. Multiple "tag=participant" checks ensure the disqualification only happens once
execute if entity @s[tag=participant] unless score @s br.game_id = current_game_id br.value run function battle_royale:disqualified/absent_for_too_long
execute if entity @s[tag=participant] if score delta var >= max_single_leave_before_disqualified br.value run function battle_royale:disqualified/absent_for_too_long
execute if entity @s[tag=participant] if score @s br.absent_for >= total_leave_before_disqualified br.value run function battle_royale:disqualified/absent_for_too_long

#Check if the player was engaged in combat before disconnecting
execute if entity @s[tag=participant] if score @s br.logging_timer matches 1.. run function battle_royale:disqualified/combat_logging

#Update dimension
function battle_royale:utility/get_player_dimension