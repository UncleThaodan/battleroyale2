#Make everyone in dimensiona participant
tag @a[tag=br.inside_arena] add participant
tag @a[tag=participant] add needs_starting_gear

#Start self-scheduling "minute interrupt"
scoreboard players operation remaining_game_duration_in_minutes br.value = max_game_duration_in_minutes br.value
schedule function battle_royale:events/a_minute_passed 60s

#Increment ID of this match
scoreboard players add current_game_id br.value 1
scoreboard players set game_is_ongoing br.value 1

scoreboard players operation @a[tag=participant] br.game_id = current_game_id br.value

# Set worldborder
execute in minecraft:map run worldborder center 12 345
execute in minecraft:map run worldborder set 600

#Remove countdown bossbar
bossbar remove lobby_countdown

#Teleport players
execute in minecraft:map run tp @a[tag=participant] 16 235 340
effect give @a[tag=participant] resistance 10 20 true

#Remove and add single-tick scoreboard to detect deaths
scoreboard objectives remove br.is_dead
scoreboard objectives add br.is_dead custom:deaths

#TODO: Give players starting gear