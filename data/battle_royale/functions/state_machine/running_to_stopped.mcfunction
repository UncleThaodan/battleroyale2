#Announce winner
tag @p[tag=participant] add victor
title @a[tag=br.inside_arena] subtitle ["",{"selector":"@p[tag=victor]"},{"text":" just won the Battle Royale!","bold":true,"color":"gold"}]
title @a[tag=br.inside_arena] title [""]

#Stop the self-scheduling "minute-interrupt"
scoreboard players set remaining_game_duration_in_minutes br.value 0
scoreboard players set game_is_ongoing br.value 0
schedule clear battle_royale:events/a_minute_passed

# Ensure everyone has left the area, even the victor
execute as @a[tag=participant] run function battle_royale:disqualify_player

execute in minecraf:map run worldborder set 10000000