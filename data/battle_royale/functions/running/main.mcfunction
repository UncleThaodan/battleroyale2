#Player has spent a tick outside of combat, decrease timer accordingly
scoreboard players remove @a[scores={br.logging_timer=1..}] br.logging_timer 1

#Disqualify dead players
execute as @a[scores={br.is_dead=1..}] run function battle_royale:disqualified/killed

#Count number of participants that are still in the game. Stop if 1 or less are left
execute store result score number_of_participants_alive var if entity @a[tag=participant]
execute if score number_of_participants_alive var matches ..1 run function battle_royale:state_machine/running_to_stopped