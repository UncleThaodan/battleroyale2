execute if score remaining_game_duration_in_minutes br.value matches 21 in minecraft:map run worldborder set 450 60
execute if score remaining_game_duration_in_minutes br.value matches 16 in minecraft:map run worldborder set 300 60
execute if score remaining_game_duration_in_minutes br.value matches 11 in minecraft:map run worldborder set 150 60
execute if score remaining_game_duration_in_minutes br.value matches 6 in minecraft:map run worldborder set 75 60
execute if score remaining_game_duration_in_minutes br.value matches 3 in minecraft:map run worldborder set 1 180