# Copy timer to work on
scoreboard players operation modulo var = remaining_game_duration_in_minutes br.value

# Remove 2 minutes if it's 4 minutes or less before the end (last segment has a 3 minute interval, not 5)
execute if score remaining_game_duration_in_minutes br.value matches ..4 run scoreboard players remove modulo var 2
scoreboard players operation modulo var %= 5 constant

# Remove 1 minute because wall movements start at 5x + 1 minutes
scoreboard players remove modulo var 1

# Copy result for the tellraw messages
scoreboard players operation battle_royale.wb_warning_minutes var = modulo var

# Take action depending on how many minutes remain until the next wall movement
# Minute 3 before the end is the last one that needs any action
execute if score remaining_game_duration_in_minutes br.value matches 3.. if score modulo var matches 1..3 run function battle_royale:running/worldborder/warnings/wall_about_to_move
execute if score remaining_game_duration_in_minutes br.value matches 3.. if score modulo var matches 0 run function battle_royale:running/worldborder/warnings/wall_now_moving
execute if score remaining_game_duration_in_minutes br.value matches 3.. if score modulo var matches 0 run function battle_royale:running/worldborder/next_wall_movement
execute if score remaining_game_duration_in_minutes br.value matches 3.. if score modulo var matches -1 run function battle_royale:running/worldborder/warnings/wall_stopped_moving