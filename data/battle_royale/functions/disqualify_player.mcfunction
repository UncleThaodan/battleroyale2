#Reset combat logging timer in case they have one
scoreboard players reset @s br.logging_timer

#Remove from participants list
tag @s remove participant

#Clear inventory, just in case
clear @s