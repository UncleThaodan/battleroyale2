#Countdown
scoreboard players remove lobby_countdown_timer br.value 1

#Update bossbar
bossbar set lobby_countdown name [{"text":"Game Starts In ","color":"gold"},{"score":{"name":"lobby_countdown_timer","objective":"br.value"},"color":"green"},{"text":" seconds","color":"gold"}]
bossbar set lobby_countdown players @a[tag=br.inside_arena]
execute store result bossbar lobby_countdown value run scoreboard players get lobby_countdown_timer br.value

#Self-schedule
execute unless score lobby_countdown_timer br.value matches 0 run schedule function battle_royale:signup/main_20t 20t
execute if score lobby_countdown_timer br.value matches ..0 run function battle_royale:signup/timer_ran_out