#Init Timer
scoreboard players operation lobby_countdown_timer br.value = countdown_before_game_start_in_seconds br.value

bossbar add lobby_countdown [{"text":"Game Starts In ","color":"gold"},{"score":{"name":"lobby_countdown_timer","objective":"br.value"},"color":"green"},{"text":" seconds","color":"gold"}]
execute store result bossbar lobby_countdown max run scoreboard players get countdown_before_game_start_in_seconds br.value
execute store result bossbar lobby_countdown value run scoreboard players get lobby_countdown_timer br.value

#Start timer
function battle_royale:signup/main_20t