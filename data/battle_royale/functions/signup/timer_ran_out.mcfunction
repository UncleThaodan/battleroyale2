#Check how many players there are and switch to the appropriate state
execute store result score available_players var if entity @a[tag=br.inside_arena]
execute if score available_players var matches 2.. run function battle_royale:state_machine/signup_to_running
execute if score available_players var matches ..1 run function battle_royale:signup/init