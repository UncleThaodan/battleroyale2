#Scratch scoreboard, can be used by any other datapack without affecting this one
scoreboard objectives add var dummy

#Never-changing values (constant numbers)
scoreboard objectives add constant dummy
scoreboard players set 5 constant 5

#Semi-persistent storage
scoreboard objectives add br.value dummy

#Number of ticks the player has been absent from the server in this game (cumulative)
scoreboard objectives add br.absent_for dummy
scoreboard objectives add br.last_online dummy

#The id of the game the player was last in (Ensures he gets disqualified when he joins a later match if he leaves the current one)
scoreboard objectives add br.game_id dummy

#Single-tick scoreboard to detect events
scoreboard objectives add br.is_dead custom:deaths

#Players that re-join the server will get a leave score of 1, which is used to detect pvp-logging or when players quit one match and re-join during another.
scoreboard objectives add br.is_rejoining custom:leave_game

#Timer counting down how many ticks the player is not allowed to disconnect for after combat, or get disqualified
scoreboard objectives add br.logging_timer dummy

#Initialise scores
scoreboard players add game_is_ongoing br.value 0

function battle_royale:settings