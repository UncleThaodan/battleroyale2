# Design-Doc / Checklist

### Game ends if
- [x] Last man standing (combat or opponent disconnect)
- [ ] Game time is up (25 Minutes) -> damage players until 1 remains

### Signup Phase
- [x] Anyone in the arena world is considered a participant
- [x] If < 2 players when timer runs out, start over
- [ ] Else begin starting procedure

### On start
- [ ] 30+ seconds "Get Ready" timer
- [x] Tag participants at start to get starter kit [needs_starting_gear]
- [x] Teleport players into arena
- [ ] Slightly separate players

### On Death
- [x] Switch to spectator
- [ ] Add way to return to lobby

### On Victory
- [x] Tag victor so they get a reward [victor]

### Announcements
- [x] Victory splash message with victor name
- [ ] "Game starting" screen
- [ ] "You have been eliminated"
- [ ] "Game in progress, please wait"

### Logging
- [x] Max logging duration: 15 seconds
- [x] Logging during combat instantly disqualifies

### Wall moves
- [x] 4-5: 600 to 450
- [x] 9-10: 450 to 300
- [x] 14-15: 300 to 150
- [x] 19-20: 150 to 75
- [x] 22-25: 75 to 1

### Overview
![](overview.svg)

##### Starter kit (Handled externally, custom names)
- 1 elytra
- 1 golden sword name
- 5 steak

##### Reward (Handled externally)
- 100 coins
- +1 Win on leaderboard
